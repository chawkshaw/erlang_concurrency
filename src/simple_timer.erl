%%%-------------------------------------------------------------------
%%% @author chawkshaw
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Apr 2014 13:40
%%%-------------------------------------------------------------------
-module(simple_timer).
-author("chawkshaw").

%% API
-export([start/2, cancel/1]).


cancel(Pid) ->
  Pid ! cancel.

start(Time, Fun) ->
  spawn(fun() -> timer(Time, Fun) end).

timer(Time, Fun) ->
  receive
    cancel ->
      void
    after Time ->
      Fun()
  end.

