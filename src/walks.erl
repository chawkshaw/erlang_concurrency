%%%-------------------------------------------------------------------
%%% @author chawkshaw
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. Apr 2014 16:28
%%%-------------------------------------------------------------------
-module(walks).
-author("chawkshaw").

%% API
-export([plan_route/2]).

-spec plan_route(From::point(),To::point()) -> route().
-type direction() :: north | south | east | west.
-type point() ::  {integer(),integer()}.
-type route() ::  {go,direction(),integer()}.



plan_route(p1, p2) ->
  {go,north,p2}.