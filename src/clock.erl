%%%-------------------------------------------------------------------
%%% @author chawkshaw
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Apr 2014 14:52
%%%-------------------------------------------------------------------
-module(clock).
-author("chawkshaw").

%% API
-export([start/2, stop/0]).


start(Time, Fun) ->
  register(clock, spawn(fun() -> tick(Time, Fun) end)).

stop() ->
  clock ! stop.

tick(Time, Fun) ->
  receive
    stop ->
      void
    after Time ->
      Fun(),
      tick(Time, Fun)
  end.