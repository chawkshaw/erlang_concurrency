%%%-------------------------------------------------------------------
%%% @author chawkshaw
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Apr 2014 13:28
%%%-------------------------------------------------------------------
-module(lib_misc).
-author("chawkshaw").

%% API
-export([]).

sleep(T) ->
  receive
    after T ->
      true
  end.

flush_buffer()  ->
  receive
    _Any  ->
      flush_buffer()
  after 0 ->
    true
  end.

priority_recieve()  ->
  receive
    {alarm, X} ->
      {alarm, X}
   after 0 ->
     receive
       Any ->
         Any
     end
  end.