%%%-------------------------------------------------------------------
%%% @author chawkshaw
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. Apr 2014 20:03
%%%-------------------------------------------------------------------
-module(area_server0).
-author("chawkshaw").

%% API
-export([loop/0]).


loop() ->
  receive
    {rectangle, Width, Ht} ->
      io:format("Area of rectangle is ~p~n", [Width * Ht]),
      loop();
    {square, Side} ->
      io:format("Area of Square is ~p~n", [Side * Side]),
      loop()
  end.
